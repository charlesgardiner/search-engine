/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cosc561.searchengine.entities;

import cosc561.searchengine.enums.UrlStatus;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.URL;
import org.jsoup.nodes.Document;

/**
 *
 * @author jraymond
 */
@Entity
@XmlRootElement
@NamedQueries(
{
    @NamedQuery(name = "Url.findAll", query = "SELECT u FROM Url u"),
    @NamedQuery(name = "Url.findById", query = "SELECT u FROM Url u WHERE u.id = :id"),
    @NamedQuery(name = "Url.findByScannedOn", query = "SELECT u FROM Url u WHERE u.scannedOn = :scannedOn"),
    @NamedQuery(name = "Url.findByStatus", query = "SELECT u FROM Url u WHERE u.status = :status")
})
public class Url implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @URL
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(nullable = false, length = 2147483647)
    private String id;
    @Column(name = "scanned_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scannedOn;
    @Size(max = 2147483647)
    @Column(length = 2147483647)
    private String document;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private UrlStatus status;
    @Version
    @Basic(optional = false)
    @Column(name = "last_update", nullable = false)
    private Timestamp lastUpdate;

    public Url()
    {
    }

    public Url(String id)
    {
        this.id = id;
    }

    public Url(String id, UrlStatus status)
    {
        this.id = id;
        this.status = status;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Date getScannedOn()
    {
        return scannedOn;
    }

    public void setScannedOn(Date scannedOn)
    {
        this.scannedOn = scannedOn;
    }

    public String getDocument()
    {
        return document;
    }

    public void setDocument(String document)
    {
        this.document = document;
    }

    public UrlStatus getStatus()
    {
        return status;
    }

    public void setStatus(UrlStatus status)
    {
        this.status = status;
    }

    public Timestamp getLastUpdate()
    {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate)
    {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Url))
            return false;
        Url other = (Url) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return id;
    }
}
