/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cosc561.searchengine.enums;

/**
 *
 * @author jraymond
 */
public enum UrlStatus
{
    NEW("New"),
    SCANNING("Scanning for urls"),
    SCANNED("Scanned"),
    PROCESSING("Processing document"),
    PROCESSED("Processed document"),
    ERROR("Error");

    private final String label;

    UrlStatus(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }
}
