/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cosc561.searchengine.cdi.viewScoped;

import cosc561.searchengine.ejb.UrlService;
import java.io.Serializable;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author jraymond
 */
@Named(value = "index")
@ViewScoped
public class index implements Serializable
{
    @Inject
    private UrlService urlService;

    /**
     * Creates a new instance of index
     */
    public index()
    {
    }

    public void runScan()
    {
        urlService.runUrlScanner();
    }
}
